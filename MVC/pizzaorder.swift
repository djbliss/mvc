//
//  pizzaorder.swift
//  MVC
//
//  Created by BLT0006-MACBK on 7/17/15.
//  Copyright (c) 2015 Blisslogix. All rights reserved.
//

import UIKit


class pizzaorder: NSObject {

  private var pizzaArray:[Pizza] = []
  private var tableNumber:[Int] = []
  func addOrder(pizza:Pizza, table:Int){
    pizzaArray.append(pizza)
    tableNumber.append(table)
  }
  func getOrder(index:Int) -> (Pizza,Int){
    return (pizzaArray[index],tableNumber[index])
  }
  func getPizzaOrder(index:Int) -> Pizza{
    return pizzaArray[index]
  }

}
