//
//  pizza.swift
//  MVC
//
//  Created by BLT0006-MACBK on 7/17/15.
//  Copyright (c) 2015 Blisslogix. All rights reserved.
//

import UIKit

class pizza: NSObject {
  class Pizza1{
    var pizzaTopping:String
    var pizzaCrust:String
    var pizzaSize:Double
    var pizzaRadius:Double {
      get{
        return pizzaSize / 2.0 }
    }
    let pi = 3.1415926
    let crust = 0.1
    init(topping:String,crust:String,size:Double){
      pizzaTopping = topping
      pizzaCrust = crust
      pizzaSize = size
    }
    func howMuchCheese(height:Double) -> Double{
      return  (pizzaRadius * pi * (1.0 - crust )) * height
    }
    }


  class PizzaOrder{
    private var pizzaArray:[Pizza1] = []
    private var tableNumber:[Int] = []
    func addOrder(pizza:Pizza1, table:Int){
      pizzaArray.append(pizza)
      tableNumber.append(table)
    }
    func getOrder(index:Int) -> (Pizza1,Int){
      return (pizzaArray[index],tableNumber[index])
    }
    func getPizzaOrder(index:Int) -> Pizza1{
      return pizzaArray[index]
    }

}
}
